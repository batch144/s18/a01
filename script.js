console.log("Hello world")

let trainer = {
  name: `Choupapi Dimagiba`,
  age: 43,
  pokemon: [
  	`Pikachu`,
  	`Charizard`,
    `Squirtle`,
  	`Bulbasaur`
  ],
  friends: {
    mobileLegends: [`Lancelot`, `Hayabusa`],
    dota: [`Slark`, `Invoker`]
  },
  talk: function(){
    console.log(`Pikachu! I choose you!`);
  }
};
console.log(trainer);

console.log(`Result of dot notation:`);
console.log(trainer.name);

console.log(`Result of square bracket notation:`);
console.log(trainer[`pokemon`]);

console.log(`Result of talk method:`);
trainer.talk();


function Pokemon(name, lvl, hp){
  this.name = name;
  this.level = lvl;
  this.health = hp * 2;
  this.attack = lvl * 100;

  this.tackle = function(target){
    console.log(target);

    console.log(`Pokemon ${this.name} tackled ${target.name}`);

    let tackledHealth = target.health - this.attack;
    console.log(`${target.name}'s is now reduced to ${tackledHealth}`)

        if (tackledHealth <= 0){
              this.faint(target);
            }
  }
  this.faint = function(target){
    console.log(`${target.name} fainted`)
  }
}

let snorlax = new Pokemon("Snorlax", 2, 300);
let eevee = new Pokemon("Eevee", 5, 100);
let piplup = new Pokemon("Piplup", 3, 140);

console.log(snorlax);
console.log(eevee);
console.log(piplup);

snorlax.tackle(piplup);
piplup.tackle(eevee)

console.log(eevee);

